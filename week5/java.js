function validate()
{
    //var forename = document.signup.forename.value;
    if ((document.signup.forename.value.indexOf(" ")) != -1) 
    {
        alert("Forename must not contain spaces");
        document.signup.forename.focus() ;
        return false;
    }

    if (document.signup.forename.value.length < 3)
    {
    	alert("The character must be at least 3 alphabet");
    	document.signup.forename.focus() ;
    	return false;

    }
    if (!(document.signup.forename.value.match(/^[a-zA-Z]+$/)))
    {
    	alert("Alphabet allow in this form only");
    	document.signup.forename.focus() ;
    	return false;

    }


    if ((document.signup.surname.value.indexOf(" ")) != -1) 
    {
        alert("Surname must not contain spaces");
        document.signup.surname.focus() ;
        return false;
    }
    if (!(document.signup.surname.value.match(/^[a-zA-Z]+$/)))
    {
    	alert("Alphabet allow in this form only");
    	document.signup.surname.focus() ;
    	return false;

    }
    if (document.signup.surname.value.length < 3)
    {
    	alert("The character must be at least 3 alphabet");
    	document.signup.surname.focus() ;
    	return false;

    }

    if (document.signup.usename.value.length < 5)
    {
    	alert("The character must be at least 5 alphabet");
    	document.signup.usename.focus() ;
    	return false;

    }

    if (!(document.signup.usename.value.match(/[0-9a-zA-Z\_\-]/)))
    {
    	alert("Alphabet allow in this form only");
    	document.signup.usename.focus() ;
    	return false;

    }


    if (document.signup.pass.value.length < 8)
    {
    	alert("The password must be at least 8 characters");
    	document.signup.pass.focus() ;
    	return false;

    }

      if (document.signup.pass.value.match(/^([0-9a-zA-Z]+$)/))
    {
    	alert(".........");
    	document.signup.pass.focus() ;
    	return false;

    }

    if (document.signup.age.value <18 || document.signup.age.value >110)
    {
    	alert("The age allow between 18-110 on this site only");
    	document.signup.age.focus() ;
    	return false;

    }

    if (!(document.signup.email.value.match(/^([0-9a-zA-Z\_\-\.])+@([a-zA-Z])+\.([a-zA-Z])/)))
    //if (!(document.signup.email.value.match(/^([0-9a-zA-Z\-\_\.])+@/)))
    {
    	alert("Email must be of the form abc@xxx.xxx only"
    		);
    	document.signup.email.focus() ;
    	return false;

    }




    
   


     /*var surname = document.forms["signup"]["surname"].value;
    if (surname == null || surname == "") {
        alert("Name must be filled out");
        return false;
    }


 	var usename = document.forms["signup"]["usename"].value;
    if (usename == null || usename == "") {
        alert("Name must be filled out");
        return false;
    }

    var pass = document.forms["signup"]["password"].value;
    if (pass == null || pass == "") {
        alert("Name must be filled out");
        return false;
    }

    var age = document.forms["signup"]["age"].value;
    if (age>=18 && age <=110 "") {
        alert("Name must be filled out");
        return false;
    }

    var email = document.forms["signup"]["email"].value;
    if (email == null || email == "") {
        alert("Name must be filled out");
        return false;
    }*/
    return true;

}